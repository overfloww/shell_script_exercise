#! bin/bash

version=8

exist=$(dpkg-query -l | grep jdk)

if [ "$exist" == "" ]; then

	echo "jdk doesn' t exist, will download jdk 1.8"

	sudo apt update && sudo apt install "openjdk-$version-jdk"
fi

javac HelloWorld.java && java HelloWorld
