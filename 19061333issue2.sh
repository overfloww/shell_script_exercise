#! bin/bash
i=0
process=''
while [ $i -le 100 ]
do
	printf "[%-50s] %d%% \r" "$process" "$i";
	sleep 0.05s
	((i=i+1))
	process+='*'
done
echo
