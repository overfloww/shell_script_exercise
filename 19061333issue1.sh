#! /bin/bash
username="tom"
password="tom"
uname=$(uname -r)
read -p "Enter username: " input_uname
read -sp "Enter password: " input_pwd
if [ ! -e loginerr.log ]; then
	touch loginerr.log
fi 

if [ ! -e login.log ]; then
	touch login.log
fi

if [ $username == $input_uname ] && [ $password == $input_pwd ]; then
	last_fail_msg=$(tail -n 1 loginerr.log)
	if [ "$last_fail_msg" != "" ]; then 
		echo -e "\nLast Failed Login:\n"
	fi
	last_succ_msg=$(tail -n 1 login.log)
	echo $last_succ_msg
	if [ "$last_succ_msg" != "" ]; then
		echo -e "\nLast Login:$last_succ_msg\n"
	fi
	echo -e "\nWelcome, $uname\n"
	echo "$(date) on $(tty)" >>login.log
	echo "" > loginerr.log
else
	echo -e "\nerror!\n"
	echo "$(date) on $(tty)" >>loginerr.log
	read
fi
