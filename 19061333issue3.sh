#! bin/bash
read -p "Enter the number: " n

for ((i=1; i <= n; i = i + 1)); do
	for ((j = 1; j<= i; j = j + 1 )); do
		if [ $j ==  1 ] || [ $j == $i ]; then
			declare a_${i}_${j}=1
		else
			declare x=$((a_$((i-1))_$j))
			declare y=$((a_$((i-1))_$((j-1))))
			declare a_${i}_${j}=$((x+y))
		fi
		printf "$((a_${i}_${j}))"
	done
	echo ""
done
